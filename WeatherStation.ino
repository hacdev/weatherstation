/*
 * Weather Station v1.0
 */

/*
 *  CONFIGURATIONS
 *  
 *  Set the global configurations in this area
 */

 /*
  * Temperature and Humidity 
  */
#include <SPI.h>
#include <Wire.h>
#include <SimpleDHT.h>


/*
 * Thunderstorm
 */
int loopCount = 0;
int thunderstorm = 0;

/*
 * Photo sensor
 */
int lightPin = 3;

int pinDHT11 = 2;       // DHT11 
SimpleDHT11 dht11;      // DHT11 

 
/*
 * Water Level Module
 */
int adc_id = 1;         // Water Level Module Pin
int level = 0;          // Current water level
int HistoryValue = 0;   // History value
char printBuffer[128];  // Buffer

/*
 * Sound Module
 */
int  sensorPin  =  3;   // select the input  pin for  the potentiometer 
int  sensorValue =  0;  // variable to  store  the value  coming  from  the sensor

/*
 * Thermo
 */
int windChillPin = 2;

/*
 *  SETUP
 *  
 *  Setup and prepare to run the service
 */
 
void setup()
{
  Serial.begin(9600);
}

/*
 *  LOOP
 *  
 *  Perform a loop of our program dumping 
 *  the data to a consumable json string
 *  via web or other wifi socket connection 
 *  on the web service port configured in the 
 *  module setup page http://192.168.4.1
 */
void loop()
{
  delay(4000);
  
  /*
   * TEMPERATURE DETECTION
   */
  // read with raw sample data.
  byte temperature = 0;
  byte humidity = 0;
  byte data[40] = {0};
  int dhres = dht11.read(pinDHT11, &temperature, &humidity, data);
  Serial.print("{\"celsius\": ");
  Serial.print(temperature); 

  int fahrenheit = ((temperature * 9)/5) + 32;
  Serial.print(", \"fah\": ");
  Serial.print(fahrenheit);
  /* END TEMPERATURE DETECTION */

  /*
   * WIND CHILL DETECTION
   */
  int tempReading = analogRead(windChillPin);
  double tempK = log(10000.0 * ((1024.0 / tempReading - 1)));
  tempK = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * tempK * tempK )) * tempK );       //  Temp Kelvin
  float tempC = tempK - 273.15;            // Convert Kelvin to Celcius
  float tempF = (tempC * 9.0)/ 5.0 + 32.0; // Convert Celcius to Fahrenheit

  Serial.print(", \"wcC\": ");
  Serial.print((int)tempC);

  Serial.print(", \"wcF\": ");
  Serial.print((int)tempF);
  /* END WIND CHILL DETECTION*/

  
  /*
   * HUMIDITY DETECTION
   */
  Serial.print(", \"humidity\": ");
  Serial.print((int)humidity);
  /* END HUMIDITY DETECTION */

  
  /*
   * THUNDERSTORM DETECTION
   */  
  sensorValue =  digitalRead(sensorPin);
  if (sensorValue == HIGH || thunderstorm == 1) {
    Serial.print(", \"ts\": ");
    Serial.print("\"Yes\"");
    thunderstorm = 1;
  } else {
    Serial.print(", \"ts\": ");
    Serial.print("\"No\"");
  }

  // 900 = 1 hr 
  // When thunder is heard, thunderstorm stays in affect for 1 hr
  if (loopCount >= 900) {
    thunderstorm = 0;
    loopCount = 0;
  }
  /* END THUNDERSTORM DETECTION */


  /*
   * RAIN LEVEL DETECTION
   */
  level = analogRead(adc_id); // get adc value

  if(((HistoryValue>=level) && ((HistoryValue - level) > 10)) || ((HistoryValue<level) && ((level - HistoryValue) > 10))) {
    //sprintf(printBuffer,"ADC%d level is %d\n",adc_id, value);
    //Serial.print(printBuffer);
    Serial.print(", \"rain\": ");
    Serial.print(level);
    HistoryValue = level;
  } else {
    Serial.print(", \"rain\": ");
    Serial.print(level);
  }
  /* END RAIN LEVEL DETECTION*/

  /*
   * LIGHT DETECTION
   */
  int lightValue  = analogRead(lightPin);
  Serial.print(", \"light\": ");
  Serial.print(lightValue);
  /* END LIGHT DETECTION */
  
  /*
   * END JSON
   */
  Serial.println("}");

}